library(rCharts)
# To install rCharts:
# require(devtools)
# install_github('rCharts', 'ramnathv')
library(leaflet)
library(maps)
library(ggplot2)
library(cowplot)
library(reshape2)

df <- read.csv("data/europe.csv", stringsAsFactors=FALSE)

# First of all, normalize the data
normalize <- function(x) {(x - min(x))/(max(x)-min(x))}

# Use lapply to apply normalize() to every column in a data frame
df.norm <- df
df.norm <- as.data.frame(lapply(df.norm[,-1], normalize))
df.norm <- cbind(df[,1], df.norm)
colnames(df.norm)[1] <- "Country"
summary(df$Country)
# Data for the radar plot
# We have to transpose it because radarchart works that way
radardata <- t(df.norm[,2:ncol(df.norm)])
colnames(radardata) <- df.norm[,1]
radardata <- as.data.frame(radardata)
#radardata["Pop.growth",] <- radardata["Pop.growth",] + 1
radardata <- radardata[-c(1,4,5),] # Remove unwanted rows: Area, Life.expect and Military
radardata$EuropeMean <-colMeans(df.norm[,-c(1,2,5,6)])
# Create the indicator variables
df.norm$IndicatorSocial <-df.norm$Healthcare+df.norm$Pop.growth-df.norm$Unemployment
df.norm$IndicatorEco <-(df.norm$GDP*4)-df.norm$Unemployment-df.norm$Inflation
df.norm$ClusterEco[df.norm$IndicatorEco < 0] <- 1
df.norm$ClusterEco[df.norm$IndicatorEco >= 0 &
                     df.norm$IndicatorEco < 1] <- 2
df.norm$ClusterEco[df.norm$IndicatorEco >= 1] <- 3
df.norm$ClusterSocial[df.norm$IndicatorSocial < 0.4] <- 1
df.norm$ClusterSocial[df.norm$IndicatorSocial >= 0.4 &
                        df.norm$IndicatorSocial < 0.93] <- 2
df.norm$ClusterSocial[df.norm$IndicatorSocial >= 0.93] <- 3
df.norm$ClusterLife[df$Life.expect < 76.5] <- 1
df.norm$ClusterLife[df$Life.expect >= 76.5 &
                      df$Life.expect <80 ] <- 2
df.norm$ClusterLife[df$Life.expect >= 80] <- 3

# Colors
barchart.colors <- rep("#555555", nrow(df))
barchart.selected <- c("#e8372e","#27b573", "#c12466", "#2460c1", "#d8bd0d", "#97e82e", "#aa17b2")

# Color scheme for the map
color.scheme = matrix(c("#CC0024", "#8a274a", "#4B264D", 
                        "#DD7C8A", "#8D6C8F", "#4A4779", 
                        "#DDDDDD", "#7BB3D1", "#016EAE"), 
                      nrow=3, 
                      ncol=3)

# Non matrix version for the legend
bvColors=c("#CC0024", "#8a274a", "#4B264D", 
           "#DD7C8A", "#8D6C8F", "#4A4779", 
           "#DDDDDD", "#7BB3D1", "#016EAE")

legendGoal=melt(matrix(1:9,nrow=3))
test<-ggplot(legendGoal, aes(Var2,Var1,fill = as.factor(value)))+ geom_tile()+
  theme(legend.position = "none") + labs(y = "Life Expect") 


# Create the plot
p3 <- nPlot(Life.expect ~ Country,
            data = df[order(-df$Life.expect),], 
            type = 'discreteBarChart'
)
p3$chart(margin = list(bottom = 100), color = barchart.colors)
p3$set(dom = 'lifeplot', width = 600, height = 400)
p3$xAxis(rotateLabels=-45)
# p3$yAxis(axisLabel = 'Life expectancy (years)', width = 60)


getCountryName <- function(id) {
  strsplit(id, ":")[[1]][1]
}
